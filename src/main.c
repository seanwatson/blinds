#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "mgos.h"
#include "mgos_gpio.h"
#include "mgos_rpc.h"
#include "mgos_sys_config.h"
#include "mgos_timers.h"

#include "buttons/bedroom_center.h"
#include "buttons/bedroom_down.h"
#include "buttons/bedroom_up.h"
#include "buttons/office_east_center.h"
#include "buttons/office_east_down.h"
#include "buttons/office_east_up.h"
#include "buttons/office_south_center.h"
#include "buttons/office_south_down.h"
#include "buttons/office_south_up.h"
#include "util.h"

#define RADIO_PIN (23)
#define SYMBOL_TIME_US (325)

typedef struct {
    const char* name;
    const size_t size;
    const uint_fast8_t* symbols;
} button_t;

typedef struct {
    button_t buttons[9];
    button_t* pressed_button;
    size_t symbol_position;
} button_group_t;

static button_group_t button_group = {
    .buttons =
        {{.name = "bedroom_center", .size = LEN(btn_bedroom_center), .symbols = btn_bedroom_center},
         {.name = "bedroom_down", .size = LEN(btn_bedroom_down), .symbols = btn_bedroom_down},
         {.name = "bedroom_up", .size = LEN(btn_bedroom_up), .symbols = btn_bedroom_up},
         {.name = "office_east_center",
          .size = LEN(btn_office_east_center),
          .symbols = btn_office_east_center},
         {.name = "office_east_down",
          .size = LEN(btn_office_east_down),
          .symbols = btn_office_east_down},
         {.name = "office_east_up", .size = LEN(btn_office_east_up), .symbols = btn_office_east_up},
         {.name = "office_south_center",
          .size = LEN(btn_office_south_center),
          .symbols = btn_office_south_center},
         {.name = "office_south_down",
          .size = LEN(btn_office_south_down),
          .symbols = btn_office_south_down},
         {.name = "office_south_up",
          .size = LEN(btn_office_south_up),
          .symbols = btn_office_south_up}},
    .pressed_button = NULL,
    .symbol_position = 0};

static int press_button(const char* button_name, button_group_t* button_group) {
    button_t* pressed_button = NULL;
    for (size_t i = 0; i < LEN(button_group->buttons); ++i) {
        if (strcmp(button_name, button_group->buttons[i].name) == 0) {
            pressed_button = &button_group->buttons[i];
            break;
        }
    }
    if (pressed_button == NULL) {
        return -1;
    }
    button_group->symbol_position = 0;
    button_group->pressed_button = pressed_button;
    return 0;
}

static void timer_cb(void* arg) {
    button_group_t* button_group = (button_group_t*)arg;

    if (button_group->pressed_button == NULL) {
        return;
    }

    mgos_gpio_write(
        RADIO_PIN, button_group->pressed_button->symbols[button_group->symbol_position]);
    button_group->symbol_position += 1;

    if (button_group->symbol_position == button_group->pressed_button->size) {
        button_group->pressed_button = NULL;

        // Make sure to turn off the radio when the button is done. All buttons
        // should have a "0" as their last symbol anyways.
        mgos_gpio_write(RADIO_PIN, false);
    }
}

static void rpc_cb(
    struct mg_rpc_request_info* ri,
    void* cb_arg,
    struct mg_rpc_frame_info* fi,
    struct mg_str args) {
    button_group_t* button_group = (button_group_t*)cb_arg;
    struct mbuf fb;
    struct json_out out = JSON_OUT_MBUF(&fb);

    mbuf_init(&fb, 20);

    if (button_group->pressed_button != NULL) {
        json_printf(&out, "{error: %Q}", "system busy");
    } else {
        char* button_name = 0;
        if (json_scanf(args.p, args.len, ri->args_fmt, &button_name) == 1) {
            json_printf(&out, "{status: %d}", press_button(button_name, button_group));
        } else {
            json_printf(&out, "{error: %Q}", "button name is required");
        }
    }

    mg_rpc_send_responsef(ri, "%.*s", fb.len, fb.buf);
    ri = NULL;

    mbuf_free(&fb);

    (void)cb_arg;
    (void)fi;
}

enum mgos_app_init_result mgos_app_init(void) {
    mgos_gpio_setup_output(RADIO_PIN, false);
    mgos_set_hw_timer(SYMBOL_TIME_US, true /* repeat */, timer_cb, &button_group);

    struct mg_rpc* c = mgos_rpc_get_global();
    mg_rpc_add_handler(c, "Blinds.PushButton", "{button_name: %Q}", rpc_cb, &button_group);

    return MGOS_APP_INIT_SUCCESS;
}
