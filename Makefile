HOSTNAME ?= 10.88.111.231

HEADERS := include/util.h
SRCS := src/main.c

DECODED_SYMBOLS_DIR := rf_samples/decoded_symbols

STYLE_ARGS := -style="{BasedOnStyle: chromium, IndentWidth: 4, AccessModifierOffset: -3, AlignAfterOpenBracket: AlwaysBreak, BinPackParameters: false, ColumnLimit: 100, SortIncludes: false}"

BUTTONS := \
	bedroom_center \
	bedroom_down \
	bedroom_up \
	office_east_center \
	office_east_down \
	office_east_up \
	office_south_center \
	office_south_down \
	office_south_up

BUTTON_HEADER_DIR := include/buttons
BUTTON_HEADERS := $(addsuffix .h,$(addprefix $(BUTTON_HEADER_DIR)/,$(BUTTONS)))
BUTTON_SYMBOLS := $(addsuffix .txt,$(addprefix $(DECODED_SYMBOLS_DIR)/,$(BUTTONS))) $(DECODED_SYMBOLS_DIR)/preamble.txt

$(BUTTON_HEADERS): $(BUTTON_SYMBOLS) scripts/header_generator.py
	for BUTTON in $(BUTTONS); do \
		scripts/header_generator.py $$BUTTON $(DECODED_SYMBOLS_DIR)/preamble.txt $(DECODED_SYMBOLS_DIR)/$$BUTTON.txt > $(BUTTON_HEADER_DIR)/$$BUTTON.h; \
	done
	clang-format -i $(STYLE_ARGS) $(BUTTON_HEADERS)

build/fw.zip: $(HEADERS) $(BUTTON_HEADERS) $(SRCS) mos.yml
	mos build --platform esp32 --local

.PHONY: headers
headers: $(BUTTON_HEADERS)

.PHONY: build
build: build/fw.zip

.PHONY: flash
flash: build/fw.zip
	mos flash

.PHONY: ota
ota: build/fw.zip
	curl -i -F filedata=@./build/fw.zip  http://$(HOSTNAME)/update
	curl -i http://$(HOSTNAME)/update/commit

.PHONY: style
style:
	clang-format -i $(STYLE_ARGS) $(HEADERS) $(SRCS)

.PHONY: clean
clean:
	rm -rf build deps
	rm -f $(BUTTON_HEADERS)
