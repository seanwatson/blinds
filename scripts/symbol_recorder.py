import argparse
import curses


def _parse_args():
    parser = argparse.ArgumentParser(
        description='Write a file with decoded 1s and 0s')

    parser.add_argument('button')

    return parser.parse_args()


def print_input(stdscr, bitstr):
    start_y = 4
    y = start_y
    row_str = ""
    for i, val in enumerate(bitstr):

        if val is None:
            val_str = '-'
        elif val:
            val_str = '1'
        else:
            val_str = '0'

        row_str += val_str

        if i % 4 == 3:
            row_str += " "
        if i % 8 == 7:
            row_str += " "

        if i % 16 == 15:
            y = start_y + (i // 16)
            stdscr.addstr(y, 0, row_str)
            row_str = ""


def output_file(button, bitstr):
    button_syms = ",".join(['1' if bit else '0' for bit in bitstr])
    with open(button + ".txt", "w") as f:
        f.write(c_header)


def main():
    args = _parse_args()

    bitstr = [None] * 30 * 8
    i = 0
    try:
        stdscr = curses.initscr()
        curses.noecho()
        curses.cbreak()

        stdscr.addstr(0, 0,
                      f"Generating symbol file for button: {args.button}")
        stdscr.addstr(2, 0,
                      "Press 1 or 0 to log next symbol (Press f to finish): ")

        i = 0
        while True:

            print_input(stdscr, bitstr)
            stdscr.refresh()

            key = stdscr.getkey()
            if key.lower() == 'q':
                break
            elif key.lower() == 'f':
                bitstr = bitstr[:i]
                output_file(args.button, bitstr)
                break
            elif key == '1':
                bitstr[i] = True
                i += 1
            elif key == '0':
                bitstr[i] = False
                i += 1

    finally:
        curses.nocbreak()
        curses.echo()
        curses.endwin()


if __name__ == '__main__':
    main()
